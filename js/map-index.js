﻿let map;
let markers = [];
let markerExists = [];
let provinces = [];
let cableLines = [];
let cableLables = [];
let cableCirles = [];
let distanceMarkers = [];
let distanceCircles = [];
let distanceLines = [];
let distanceLabels = [];
let mapCenter = { lat: 10.052636787078676, lng: 105.12921757069103 };
let menuDisplayed = false;

/**INIT MAP */
function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        center: mapCenter,
        zoom: 9,
        disableDefaultUI: true
        //zoomControlOptions: {
        //    style: google.maps.ZoomControlStyle.SMALL,
        //    position: google.maps.ControlPosition.RIGHT_TOP
        //},
        //mapTypeControl: true,
        //mapTypeControlOptions: {
        //    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        //    position: google.maps.ControlPosition.TOP_LEFT,
        //},
        //zoomControl: true
    });

    initMapTypeControl(map);
    initFullscreenControl(map);
    initZoomControl(map);
    resetControl(map);
    printControl(map);
}


//REPLACE CONTROL

const removeData = () => {

    markers.forEach(function (marker) {
        marker.marker.setMap(null);
    });
    markers = [];

    distanceLabels.forEach(function (label) {
        label.setMap(null);
    });
    distanceLabels = [];

    distanceLines.forEach(function (line) {
        line.setMap(null);
    });
    distanceLines = [];

    distanceCircles.forEach(function (circle) {
        circle.setMap(null);
    });
    distanceCircles = [];

    cableLines.forEach(function (line) {
        line.setMap(null);
    });
    cableLines = [];

    cableLables.forEach(function (label) {
        label.setMap(null);
    });
    cableLables = [];

    cableCirles.forEach(function (circle) {
        circle.setMap(null);
    });
    cableCirles = [];

    distanceMarkers = [];

    markerExists = [];

    showModalDistance();

}

const resetMap = () => {

    if ($('#jstreeProvince').jstree('get_selected').length > 0) {
        $("#jstreeProvince").jstree().uncheck_all(true);
    }
    $("#jstreeProvince").jstree('close_all');

    removeData();

    map.setCenter(mapCenter);
    map.setZoom(9);
}

function resetControl(map) {
    document.querySelector(".reset-control-icon").onclick = function () {
        resetMap();
    };

    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(
        document.querySelector(".reset-control")
    );
}


function printControl(map) {
    document.querySelector(".print-control-icon").onclick = function () {
        window.print();
    };

    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(
        document.querySelector(".print-control")
    );
}


function initZoomControl(map) {
    document.querySelector(".zoom-control-in").onclick = function () {
        map.setZoom(map.getZoom() + 1);
    };

    document.querySelector(".zoom-control-out").onclick = function () {
        map.setZoom(map.getZoom() - 1);
    };

    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(
        document.querySelector(".zoom-control")
    );
}

function initMapTypeControl(map) {
    const mapTypeControlDiv = document.querySelector(".maptype-control");

    document.querySelector(".maptype-control-map").onclick = function () {
        mapTypeControlDiv.classList.add("maptype-control-is-map");
        mapTypeControlDiv.classList.remove("maptype-control-is-satellite");
        map.setMapTypeId("roadmap");
        $('[name="radTypeMap"][value="roadmap"]').prop('checked', true);
    };

    document.querySelector(".maptype-control-satellite").onclick = function () {
        mapTypeControlDiv.classList.remove("maptype-control-is-map");
        mapTypeControlDiv.classList.add("maptype-control-is-satellite");
        map.setMapTypeId("hybrid");
        $('[name="radTypeMap"][value="hybrid"]').prop('checked', true);
    };

    map.controls[google.maps.ControlPosition.LEFT_TOP].push(mapTypeControlDiv);
}

function initFullscreenControl(map) {
    const elementToSendFullscreen = map.getDiv().firstChild;
    const fullscreenControl = document.querySelector(".fullscreen-control");

    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(fullscreenControl);
    fullscreenControl.onclick = function () {
        if (isFullscreen(elementToSendFullscreen)) {
            exitFullscreen();
        } else {
            requestFullscreen(elementToSendFullscreen);
        }
    };

    document.onwebkitfullscreenchange =
        document.onmsfullscreenchange =
        document.onmozfullscreenchange =
        document.onfullscreenchange =
        function () {
            if (isFullscreen(elementToSendFullscreen)) {
                fullscreenControl.classList.add("is-fullscreen");
            } else {
                fullscreenControl.classList.remove("is-fullscreen");
            }
        };
}

function isFullscreen(element) {
    return (
        (document.fullscreenElement ||
            document.webkitFullscreenElement ||
            document.mozFullScreenElement ||
            document.msFullscreenElement) == element
    );
}

function requestFullscreen(element) {
    if (element.requestFullscreen) {
        element.requestFullscreen();
    } else if (element.webkitRequestFullScreen) {
        element.webkitRequestFullScreen();
    } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if (element.msRequestFullScreen) {
        element.msRequestFullScreen();
    }
}

function exitFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    }
}
//END REPLACE

/**
 * ADD MARKER TO MAP
 * @param {any} item
 */
const addMarkerToMap = (item) => {

    const marker = new google.maps.Marker({
        position: new google.maps.LatLng(item.lat, item.lng),
        //animation: google.maps.Animation.DROP,
        label: {
            text: item.text,
            className: 'marker-label '
        },
        icon: {
            url: 'http://hoasinhnamviet.thietkevuondao.com/Content/View/img/broadcast.png',
            scaledSize: new google.maps.Size(25, 25),
            labelOrigin: new google.maps.Point(15, 30),
            //origin: new google.maps.Point(0, 0),
            //anchor: new google.maps.Point(11, 40),
            //uniqueSrc: 'icon.url'
        },
        id: item.id,
        draggable: false,
        map: map,
    });

    const infowindow = new google.maps.InfoWindow({
        content: customInfoWindow(item),
    });

    marker.addListener("click", () => {
        infowindow.open({
            anchor: marker,
            map,
            shouldFocus: false,
        });

        map.setCenter(marker.getPosition());
    });

    marker.addListener("rightclick", function (e) {
        for (prop in e) {
            if (e[prop] instanceof MouseEvent) {
                mouseEvt = e[prop];
                mouseEvt.preventDefault();
                showContextMenu(item.id, mouseEvt.clientY, mouseEvt.clientX);
            }
        }
    });

    map.addListener("click", () => {
        closeContextMenu();
        infowindow.close();
    });

    markers.push({ id: item.id, item: item, marker: marker });
    markerExists.push(item.lat + ':' + item.lng);
}

/**SHOW MODAL DISATANCE */
const showModalDistance = () => {
    distanceLines.forEach(function (line) {
        line.setMap(null);
    });
    distanceLines = [];

    distanceLabels.forEach(function (label) {
        label.setMap(null);
    });
    distanceLabels = [];

    distanceCircles.forEach(function (circle) {
        circle.setMap(null);
    });
    distanceCircles = [];

    let totalMarker = distanceMarkers.length;
    let totalDistance = 0;
    let textArrayDistance = [];
    let lineMarkerDistance = [];
    let html = '';

    $('#mapInfoModal .mapinfomodal-header').html('Đo khoảng cách');

    if (totalMarker === 0) {
        $('#mapInfoModal').hide();
    } else if (totalMarker === 1) {
        $('#mapInfoModal').fadeIn();
        let marker = findMarkerByID(distanceMarkers[0]);
        html += '<div>- Trạm đầu: <strong>' + marker.item.text + '</strong></div>';
        html += '<div>- Vui lòng chọn trạm tiếp theo để đo...</div>';
    } else if (totalMarker > 1) {
        $('#mapInfoModal').fadeIn();
        let prevMarker = null;
        for (i = 0; i <= totalMarker - 1; i++) {
            let marker = findMarkerByID(distanceMarkers[i]);
            lineMarkerDistance.push(marker.marker.getPosition());
            if (i === 0) {
                html += '<div>- Trạm đầu: <strong>' + marker.item.text + '</strong></div>';
                prevMarker = marker;
            } else if (i === totalMarker - 1) {
                html += '<div>- Trạm cuối: <strong>' + marker.item.text + '</strong></div>';
                let distance = computeDistanceBetween(prevMarker.marker.getPosition(), marker.marker.getPosition());
                totalDistance += distance;
                textArrayDistance.push('<div>' + prevMarker.item.text + ' <i class="fas fa-arrow-right"></i> ' + marker.item.text + ': <strong>' + distance.toFixed(2) + ' km</strong></div>');
                prevMarker = marker;
            } else {
                html += '<div>- Trạm giữa: <strong>' + marker.item.text + '</strong></div>';
                let distance = computeDistanceBetween(prevMarker.marker.getPosition(), marker.marker.getPosition());
                totalDistance += distance;
                textArrayDistance.push('<div>' + prevMarker.item.text + ' <i class="fas fa-arrow-right"></i> ' + marker.item.text + ': <strong>' + distance.toFixed(2) + ' km</strong></div>');
                prevMarker = marker;
            }
        }
    }

    if (totalMarker === 2) {
        html += '<hr><div>* Khoảng cách: <strong>' + totalDistance.toFixed(2) + ' km</strong></div>';
    } else if (totalMarker > 2) {
        html += '<hr>' + textArrayDistance.join('');
        html += '<div>* Tổng khoảng cách: <strong>' + totalDistance.toFixed(2) + ' km</strong></div>';
    }

    $('#mapInfoModal .mapinfomodal-body').html(html);


    const lineSymbol = {
        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
        strokeColor: "#ff0000",
    };


    for (i = 1; i <= lineMarkerDistance.length - 1; i++) {

        var latLabel = (lineMarkerDistance[i - 1].lat() + lineMarkerDistance[i].lat()) / 2;
        var lngLabel = (lineMarkerDistance[i - 1].lng() + lineMarkerDistance[i].lng()) / 2;

        let distance = computeDistanceBetween(lineMarkerDistance[i - 1], lineMarkerDistance[i]);

        let line = new google.maps.Polyline({
            path: [lineMarkerDistance[i - 1], lineMarkerDistance[i]],
            icons: [
                {
                    icon: lineSymbol,
                    offset: "100%",
                },
            ],
            map: map,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 0.5,
            strokeWeight: 4
        });

        var mapLabel = new MapLabel({
            text: distance.toFixed(2) + ' km',
            position: new google.maps.LatLng(latLabel, lngLabel),
            map: map,
            fontSize: 16,
            align: 'right'
        });


        distanceLabels.push(mapLabel);
        distanceLines.push(line);
        animateCircle(line);
    }


    for (i = 0; i <= totalMarker - 1; i++) {
        let marker = findMarkerByID(distanceMarkers[i]);
        const circle = new google.maps.Circle({
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
            map,
            center: marker.marker.getPosition(),
            radius: Math.sqrt(70) * 100,
        });
        distanceCircles.push(circle);
    }
}

const animateCircle = (line) => {
    let count = 0;

    window.setInterval(() => {
        count = (count + 1) % 200;

        const icons = line.get("icons");

        icons[0].offset = count / 2 + "%";
        line.set("icons", icons);
    }, 20);
}

/**
 * SHOW MENU CONTEXT
 * @param {any} id
 * @param {any} top
 * @param {any} left
 */
const showContextMenu = (id, top, left) => {
    $('#markermenu').attr('data-marker-id', id);
    $('#markermenu').css({
        'top': top,
        'left': left
    });
    $('#markermenu').show();
    menuDisplayed = true;

    $('#markermenu .menu-item').unbind('click');
    $('#markermenu .menu-item').each(function () {
        $(this).click(function () {

            let action = $(this).attr('data-action');
            let markerID = $(this).parent().attr('data-marker-id');

            if (action === 'start_distance') {
                distanceMarkers = [];
                distanceMarkers.push(markerID);
            } else if (action === 'end_distance') {
                distanceMarkers.push(markerID);
            } else if (action === 'cancel_distance') {
                distanceMarkers = [];
            }

            closeContextMenu();
            showModalDistance();

        });
    });
}


/**CLOSE MENU CONTEXT */
const closeContextMenu = () => {
    if (menuDisplayed === true) {
        $('#markermenu').hide();
        menuDisplayed = false;
    }
}

/**
 * CUSTOM INFO MODAL
 * @param {any} item
 */
const customInfoWindow = (item) => {
    let contentString = '';
    contentString =
        '<div class="info-map">' +
        '<table border="0" cellpading="0" cellspacing="0">' +
        '   <tr>' +
        '       <td rowspan="5" width="155"><img width="155" src="https://via.placeholder.com/300x300/cccccc/000000?text=' + item.text + '"/></td>' +
        '       <td width="130"><strong>Mã cột/trạm:</strong></td>' +
        '       <td>' + item.text + '</td>' +
        '   </tr>' +
        '   <tr>' +
        '       <td><strong>Nhà mạng:</strong></td>' +
        '       <td>' + item.network + '</td>' +
        '   </tr>' +
        '   <tr>' +
        '       <td><strong>Công nghệ:</strong></td>' +
        '       <td>' + item.networktype + '</td>' +
        '   </tr>' +
        '   <tr>' +
        '       <td><strong>Xã/phường:</strong></td>' +
        '       <td><div class="info-map-address">' + item.diachi + '</div></td>' +
        '   </tr>' +
        '   <tr>' +
        '       <td><strong>Thông tin:</strong></td>' +
        '       <td><a target="_blank" href="http://google.com">Xem chi tiết</a></td>' +
        '   </tr>' +
        '</table>' +
        '</div>';

    /*
     '<div class="info-map-title">' + item.text + '</div>' +
        '<div class="info-map-content ">' + item.diachi + '</div>' +
     */
    return contentString;
}

/**
 * LID DISTANCE
 * @param {any} mak1
 * @param {any} mak2
 */
const computeDistanceBetween = (mak1, mak2) => {
    var distance = google.maps.geometry.spherical.computeDistanceBetween(mak1, mak2); //met
    return distance / 1000; // km
}

/**
 * LID FIND MARKER BY ID
 * @param {any} value
 */
const findMarkerByID = (value) => {
    for (var i in markers) {
        if (markers[i].id == value) {
            return markers[i];
        }
    }
    return null;
}

/**
 * LOAD BTS BY PROVINCE
 * @param {any} arrayID
 */
//let bts = [];
const loadBTS = (arrayID) => {
    removeData();
    if (arrayID.length > 0) {
        $.ajax({
            url: '/JsonFiles/bts.json?ran=' + Math.random(),
            dataType: 'JSON',
            cache: 'false',
            success: function (response) {
                //bts = bts.concat(response);
                //$.each(response, function (i, item) {
                //    var provinceItem = filterByKey(provinces, 'id', item.provinceid);
                //    item.diachi = provinceItem[0].text;
                //});
                //console.log(response);
                var filterBTS = filterByKeys(response, 'provinceid', arrayID);
                $.each(filterBTS, function (i, item) {
                    if (markerExists.indexOf(item.lat + ':' + item.lng) === -1) {
                        addMarkerToMap(item);
                    }
                });
            }
        });
    }
}

const loadCableLine = () => {

    removeData();

    var cableIDs = $('[name="chkQuyHoach[]"]:checked').map(function () {
        return $(this).val();
    }).get();

    if (cableIDs.length > 0) {

        $.ajax({
            url: '/JsonFiles/tuyencap.json?ran=' + Math.random(),
            dataType: 'JSON',
            cache:"false",
            success: function (response) {

                //cableLines.forEach(function (line) {
                //    line.setMap(null);
                //});
                //cableLines = [];

                //cableLables.forEach(function (label) {
                //    label.setMap(null);
                //});
                //cableLables = [];

                //cableCirles.forEach(function (circle) {
                //    circle.setMap(null);
                //});
                //cableCirles = [];

                var filterCables = filterByKeys(response, 'typeid', cableIDs);

                $.each(filterCables, function (i, v) {
                    drawCableLine(v);
                });
            }
        });
    }
}

const drawCableLine = (item) => {

    let pathLocation = [];
    item.location.forEach(function (v) {
        pathLocation.push(new google.maps.LatLng(v[0], v[1]));
    });

    var latLabel = (pathLocation[0].lat() + pathLocation[1].lat()) / 2;
    var lngLabel = (pathLocation[0].lng() + pathLocation[1].lng()) / 2;

    let line = new google.maps.Polyline({
        path: pathLocation,
        //icons: [
        //    {
        //        icon: lineSymbol,
        //        offset: "100%",
        //    },
        //],
        map: map,
        geodesic: true,
        strokeColor: item.color,
        strokeOpacity: 0.5,
        strokeWeight: 10
    });

    google.maps.event.addListener(line, 'mouseover', function (latlng) {
        line.setOptions({ strokeOpacity: 1, strokeWeight: 14 });
    });

    google.maps.event.addListener(line, 'mouseout', function (latlng) {
        line.setOptions({ strokeOpacity: 0.5, strokeWeight: 10 });
    });

    var mapLabel = new MapLabel({
        text: item.text,
        position: new google.maps.LatLng(latLabel, lngLabel),
        map: map,
        fontSize: 16,
        fontColor: item.color,
        align: 'left',
        zIndex: 99999
    });

    const circleFirst = new google.maps.Circle({
        strokeColor: item.color,
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: item.color,
        fillOpacity: 0.35,
        map,
        center: pathLocation[0],
        radius: Math.sqrt(40) * 100,
    });
    const circleEnd = new google.maps.Circle({
        strokeColor: item.color,
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: item.color,
        fillOpacity: 0.35,
        map,
        center: pathLocation[pathLocation.length - 1],
        radius: Math.sqrt(40) * 100,
    });

    cableCirles.push(circleFirst);
    cableCirles.push(circleEnd);
    cableLables.push(mapLabel);
    cableLines.push(line);
}


/**INIT TREE */
const initTreeProvince = () => {

    $.ajax({
        "url": "/JsonFiles/provinces.json",
        "dataType": "json",
        "cache": false,
        success: function (response) {
            provinces = response;

            $("#jstreeProvince")
                .on("changed.jstree", function (e, data) {
                    loadBTS(data.selected);
                }).jstree({
                    "plugins": ["checkbox"],
                    'core': {
                        "data": provinces
                        //'data': {
                        //    "url": "/JsonFiles/provinces.json",
                        //    "dataType": "json",
                        //    "cache": false
                        //}
                    }
                });
        }
    });
}

/**RESIZE BOX MAP */
const resizeBoxMap = () => {

    var hHeader = $('#content > nav').outerHeight();

    $('.boxMap').css('height', ' calc( 100vh - ' + parseInt(hHeader) + 'px)');
    $('.boxMapSideBar').css('height', ' calc( 100vh - ' + parseInt(hHeader) + 'px)');


}

/**FILTER PROVINCE */
const filterProvince = () => {
    let query = $('#txtMapFilter').val();
    if (query !== '' && query.length > 0) {
        let resultFilters = filterByToken(provinces, query, 'text');
        let resultDisplay = JSON.parse(JSON.stringify(resultFilters));
        $.each(resultDisplay, function (i, v) {
            v.parent = '#';
        });
        $('#btnClearFilter').fadeIn();
        $('#jstreeProvince').jstree(true).settings.core.data = resultDisplay;
    } else {
        $('#btnClearFilter').hide();
        $('#jstreeProvince').jstree(true).settings.core.data = provinces;
    }
    $('#jstreeProvince').jstree(true).refresh();
}
const clearFilter = () => {
    $('#txtMapFilter').val('');
    filterProvince();
}


const showMap = (e, type) => {
    resetMap();
    $('ul.map-menu-left-icon li a').removeClass('active');
    if (type === 'map') {
        $(e).addClass('active');
        $('.map-province').show();
        $('.map-layer').hide();
    } else {
        $(e).addClass('active');
        $('.map-province').hide();
        $('.map-layer').show();
    }
    $('.boxMap').removeClass('hidemapsidebar');
    return false;
}


const hideMap = (e, type) => {

    if (type === 'map') {
        $(e).parents('.map-province').hide();
    } else {
        $(e).parents('.map-layer').hide();
    }
    $('.boxMap').addClass('hidemapsidebar');
    return false;
}

/** DOCUMENT READEY*/
$(function () {

    initMap();

    initTreeProvince();
    resizeBoxMap();
    $(window).resize(function () {
        resizeBoxMap();
    });


    ///close modal distance
    $('.mapinfomodal-close').click(() => {
        distanceMarkers = [];
        closeContextMenu();
        showModalDistance();
    });


    ///disable right click
    const noRightClick = document.getElementsByClassName("map-right")[0];
    noRightClick.addEventListener("contextmenu", e => e.preventDefault());

    ///filter
    $('#txtMapFilter').on('focus', function (e) {
        $(this).one('mouseup', function () {
            $(this).select();
            return false;
        }).select();
    }).on('keyup', function (e) {
        var keycode = e.keyCode || e.which;
        if ([13, 27, 37, 38, 39, 40].indexOf(keycode) == -1) {
            var text = $(this).val();
            isTypeHeadProcessing = false
            if (timeOutTypeHead != null) {
                clearTimeout(timeOutTypeHead);
            }
            timeOutTypeHead = setTimeout(function () {
                timeOutTypeHead = null;
                isTypeHeadProcessing = true;
                filterProvince();
            }, 300);
        }
    });

    ///
    $('[name="radTypeMap"]').click(function () {
        var typeMap = $('[name="radTypeMap"]:checked').val();

        const mapTypeControlDiv = document.querySelector(".maptype-control");
        if (typeMap === 'roadmap') {
            mapTypeControlDiv.classList.add("maptype-control-is-map");
            mapTypeControlDiv.classList.remove("maptype-control-is-satellite");
        } else if (typeMap === 'hybrid') {
            mapTypeControlDiv.classList.remove("maptype-control-is-map");
            mapTypeControlDiv.classList.add("maptype-control-is-satellite");
        }

        map.setMapTypeId(typeMap);
    });

    ///
    $('[name="chkQuyHoach[]"]').click(function () {

        loadCableLine();

    });


    $('#jstreeLayer li div.title').each(function () {
        $(this).find('.toggleSubMenu').click(function () {
            let elParent = $(this).parent();
            if (elParent.hasClass('active')) {
                elParent.next().slideUp(function () {
                    elParent.removeClass('active');
                });
                $(this).html('<i class="fas fa-plus"></i>');
            } else {
                elParent.next().slideDown();
                elParent.addClass('active');
                $(this).html('<i class="fas fa-minus"></i>');
            }
        });
    });

    if (mobileAndTabletCheck()) {
        $('.map-province').hide();
        $('.map-layer').hide();
        $('.boxMap').addClass('hidemapsidebar');
    }

});

/** FILTER OBJECTS */

var trim = function (str) {
    return (str + '').replace(/^\s+|\s+$|/g, '');
};

var escape_regex = function (str) {
    return (str + '').replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1');
};

var DIACRITICS = {
    'a': '[aḀḁĂăÂâǍǎȺⱥȦȧẠạÄäÀàÁáĀāÃãÅåąĄÃąĄ]',
    'b': '[b␢βΒB฿𐌁ᛒ]',
    'c': '[cĆćĈĉČčĊċC̄c̄ÇçḈḉȻȼƇƈɕᴄＣｃ]',
    'd': '[dĎďḊḋḐḑḌḍḒḓḎḏĐđD̦d̦ƉɖƊɗƋƌᵭᶁᶑȡᴅＤｄð]',
    'e': '[eÉéÈèÊêḘḙĚěĔĕẼẽḚḛẺẻĖėËëĒēȨȩĘęᶒɆɇȄȅẾếỀềỄễỂểḜḝḖḗḔḕȆȇẸẹỆệⱸᴇＥｅɘǝƏƐε]',
    'f': '[fƑƒḞḟ]',
    'g': '[gɢ₲ǤǥĜĝĞğĢģƓɠĠġ]',
    'h': '[hĤĥĦħḨḩẖẖḤḥḢḣɦʰǶƕ]',
    'i': '[iÍíÌìĬĭÎîǏǐÏïḮḯĨĩĮįĪīỈỉȈȉȊȋỊịḬḭƗɨɨ̆ᵻᶖİiIıɪＩｉ]',
    'j': '[jȷĴĵɈɉʝɟʲ]',
    'k': '[kƘƙꝀꝁḰḱǨǩḲḳḴḵκϰ₭]',
    'l': '[lŁłĽľĻļĹĺḶḷḸḹḼḽḺḻĿŀȽƚⱠⱡⱢɫɬᶅɭȴʟＬｌ]',
    'n': '[nŃńǸǹŇňÑñṄṅŅņṆṇṊṋṈṉN̈n̈ƝɲȠƞᵰᶇɳȵɴＮｎŊŋ]',
    'o': '[oØøÖöÓóÒòÔôǑǒŐőŎŏȮȯỌọỘộƟɵƠơỎỏŌōÕõǪǫȌȍՕօ]',
    'p': '[pṔṕṖṗⱣᵽƤƥᵱ]',
    'q': '[qꝖꝗʠɊɋꝘꝙq̃]',
    'r': '[rŔŕɌɍŘřŖŗṘṙȐȑȒȓṚṛⱤɽ]',
    's': '[sŚśṠṡṢṣꞨꞩŜŝŠšŞşȘșS̈s̈]',
    't': '[tŤťṪṫŢţṬṭƮʈȚțṰṱṮṯƬƭ]',
    'u': '[uŬŭɄʉỤụÜüÚúÙùÛûǓǔŰűŬŭƯưỦủŪūŨũŲųȔȕ∪]',
    'v': '[vṼṽṾṿƲʋꝞꝟⱱʋ]',
    'w': '[wẂẃẀẁŴŵẄẅẆẇẈẉ]',
    'x': '[xẌẍẊẋχ]',
    'y': '[yÝýỲỳŶŷŸÿỸỹẎẏỴỵɎɏƳƴ]',
    'z': '[zŹźẐẑŽžŻżẒẓẔẕƵƶ]'
};


String.prototype.tokenSearch = function () {
    query = this;
    query = trim(String(query || '').toLowerCase());
    if (!query || !query.length) return [];

    var i, n, regex, letter;
    var tokens = [];
    var words = query.split(/ +/);

    for (i = 0, n = words.length; i < n; i++) {
        regex = escape_regex(words[i]);
        if (true) {
            for (letter in DIACRITICS) {
                if (DIACRITICS.hasOwnProperty(letter)) {
                    regex = regex.replace(new RegExp(letter, 'g'), DIACRITICS[letter]);
                }
            }
        }

        tokens.push({
            string: words[i],
            regex: new RegExp(regex, 'i')
        });
    }

    return tokens;
};

var isTypeHeadProcessing = true;
var timeOutTypeHead = null;

const filterByToken = (objs, query, key) => {
    var results = [];

    var keyWord = String(query || '').toLowerCase();

    var tokens = keyWord.tokenSearch();

    var token_count = tokens.length;

    var arrayKey = String(key || '').split(',');

    for (o = 0; o < objs.length; o++) {

        if (!isTypeHeadProcessing) {
            break;
        }
        var obj = objs[o];

        if (true) {
            var score = 0;
            var text = '';

            for (j = 0; j < arrayKey.length; j++) {
                text += String(obj[arrayKey[j]] || '').toLowerCase() + ' ';
            }

            for (var i = 0, sum = 0; i < token_count; i++) {
                if (text.search(tokens[i].regex) > -1) {
                    for (j = 0; j < arrayKey.length; j++) {
                        var keyValue = String(obj[arrayKey[j]] || '');
                        if (keyValue != '' && keyValue.search(tokens[i].regex) > -1) {
                            score += tokens[i].string.length / keyValue.length;
                        }
                    }
                }
            }

            if (score > 0) {
                obj.score = score;
                results.push(obj);
            }
        } else {
            break;
        }
    }

    results.sort(function (a, b) {
        var x = b.score, y = a.score;
        return x < y ? -1 : x > y ? 1 : 0;
    });

    return results;
}

/**
 * FILTER BY KEY
 * @param {any} obj
 * @param {any} key
 * @param {any} value
 */
const filterByKey = (obj, key, value) => {
    var result = [];
    for (var i in obj) {
        if (obj[i][key] == value) {
            result.push(obj[i]);
        }
    }
    return result;
}
const filterByKeys = (obj, key, arrayValue) => {
    var result = [];
    for (var i in obj) {
        if (arrayValue.indexOf(obj[i][key]) > -1) {
            result.push(obj[i]);
        }
    }
    return result;
}
